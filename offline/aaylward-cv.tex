%-----------------------------------------------------------------------------------------------------------------------------------------------%
%	The MIT License (MIT)
%
%	Copyright (c) 2021 Jan Küster, Anthony Aylward
%
%	Permission is hereby granted, free of charge, to any person obtaining a copy
%	of this software and associated documentation files (the "Software"), to deal
%	in the Software without restriction, including without limitation the rights
%	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
%	copies of the Software, and to permit persons to whom the Software is
%	furnished to do so, subject to the following conditions:
%	
%	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
%	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
%	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
%	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
%	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
%	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
%	THE SOFTWARE.
%	
%
%-----------------------------------------------------------------------------------------------------------------------------------------------%


%============================================================================%
%
%	DOCUMENT DEFINITION
%
%============================================================================%

%we use article class because we want to fully customize the page and don't use a cv template
\documentclass[10pt,A4]{article}	


%----------------------------------------------------------------------------------------
%	ENCODING
%----------------------------------------------------------------------------------------

% we use utf8 since we want to build from any machine
\usepackage[utf8]{inputenc}		

%----------------------------------------------------------------------------------------
%	LOGIC
%----------------------------------------------------------------------------------------

% provides \isempty test
\usepackage{xstring, xifthen}

%----------------------------------------------------------------------------------------
%	FONT BASICS
%----------------------------------------------------------------------------------------

% some tex-live fonts - choose your own

%\usepackage[defaultsans]{droidsans}
%\usepackage[default]{comfortaa}
%\usepackage{cmbright}
\usepackage[default]{raleway}
%\usepackage{fetamont}
%\usepackage[default]{gillius}
%\usepackage[light,math]{iwona}
%\usepackage[thin]{roboto} 

% set font default
\renewcommand*\familydefault{\sfdefault} 	
\usepackage[T1]{fontenc}

% more font size definitions
\usepackage{moresize}

%----------------------------------------------------------------------------------------
%	FONT AWESOME ICONS
%---------------------------------------------------------------------------------------- 

% include the fontawesome icon set
\usepackage{fontawesome}

% use to vertically center content
% credits to: http://tex.stackexchange.com/questions/7219/how-to-vertically-center-two-images-next-to-each-other
\newcommand{\vcenteredinclude}[1]{\begingroup
\setbox0=\hbox{\includegraphics{#1}}%
\parbox{\wd0}{\box0}\endgroup}

% use to vertically center content
% credits to: http://tex.stackexchange.com/questions/7219/how-to-vertically-center-two-images-next-to-each-other
\newcommand*{\vcenteredhbox}[1]{\begingroup
\setbox0=\hbox{#1}\parbox{\wd0}{\box0}\endgroup}

% icon shortcut
\newcommand{\icon}[3] { 							
	\makebox(#2, #2){\textcolor{contactcol}{\csname fa#1\endcsname}}
}	

% icon with text shortcut
\newcommand{\icontext}[4]{ 						
	\vcenteredhbox{\icon{#1}{#2}{#3}}  \hspace{2pt}  \parbox{0.9\mpwidth}{\textcolor{#4}{#3}}
}

% icon with website url
\newcommand{\iconhref}[5]{ 						
    \vcenteredhbox{\icon{#1}{#2}{#5}}  \hspace{2pt} \href{#4}{\textcolor{#5}{#3}}
}

% icon with email link
\newcommand{\iconemail}[5]{ 						
    \vcenteredhbox{\icon{#1}{#2}{#5}}  \hspace{2pt} \href{mailto:#4}{\textcolor{#5}{#3}}
}

%----------------------------------------------------------------------------------------
%	PAGE LAYOUT  DEFINITIONS
%----------------------------------------------------------------------------------------

% page outer frames (debug-only)
% \usepackage{showframe}		

% we use paracol to display breakable two columns
\usepackage{paracol}

% define page styles using geometry
\usepackage[a4paper]{geometry}

% remove all possible margins
\geometry{top=1cm, bottom=1cm, left=1cm, right=1cm}

\usepackage{fancyhdr}
\pagestyle{empty}

% space between header and content
% \setlength{\headheight}{0pt}

% indentation is zero
\setlength{\parindent}{0mm}

%----------------------------------------------------------------------------------------
%	TABLE /ARRAY DEFINITIONS
%---------------------------------------------------------------------------------------- 

% extended aligning of tabular cells
\usepackage{array}

% custom column right-align with fixed width
% use like p{size} but via x{size}
\newcolumntype{x}[1]{%
>{\raggedleft\hspace{0pt}}p{#1}}%


%----------------------------------------------------------------------------------------
%	GRAPHICS DEFINITIONS
%---------------------------------------------------------------------------------------- 

%for header image
\usepackage{graphicx}

% use this for floating figures
% \usepackage{wrapfig}
% \usepackage{float}
% \floatstyle{boxed} 
% \restylefloat{figure}

%for drawing graphics		
\usepackage{tikz}				
\usetikzlibrary{shapes, backgrounds,mindmap, trees}

%----------------------------------------------------------------------------------------
%	Color DEFINITIONS
%---------------------------------------------------------------------------------------- 
\usepackage{transparent}
\usepackage{color}

% primary color
\definecolor{maincol}{RGB}{ 228, 26, 28 }

% accent color, secondary
% \definecolor{accentcol}{RGB}{ 250, 150, 10 }

% dark color
\definecolor{darkcol}{RGB}{ 70, 70, 70 }

% light color
\definecolor{lightcol}{RGB}{242, 242, 242}

% skills color
\definecolor{skillcol}{RGB}{ 55, 126, 184 }
% \definecolor{skillcol}{RGB}{ 228, 26, 28 }

% contact color
\definecolor{contactcol}{RGB}{ 228, 26, 28 }
% \definecolor{contactcol}{RGB}{ 55, 126, 184 }

% education color
\definecolor{educol}{RGB}{ 77, 175, 74 }

% experience color
\definecolor{expcol}{RGB}{ 255, 127, 0 }
% \definecolor{expcol}{RGB}{ 255, 127, 0 }

% publications color
\definecolor{pubcol}{RGB}{ 152, 78, 163  }
% \definecolor{pubcol}{RGB}{ 166, 86, 40 }


% Package for links, must be the last package used
\usepackage[hidelinks]{hyperref}

% returns minipage width minus two times \fboxsep
% to keep padding included in width calculations
% can also be used for other boxes / environments
\newcommand{\mpwidth}{\linewidth-\fboxsep-\fboxsep}
	


%============================================================================%
%
%	CV COMMANDS
%
%============================================================================%

%----------------------------------------------------------------------------------------
%	 CV LIST
%----------------------------------------------------------------------------------------

% renders a standard latex list but abstracts away the environment definition (begin/end)
\newcommand{\cvlist}[1] {
	\begin{itemize}{#1}\end{itemize}
}

%----------------------------------------------------------------------------------------
%	 CV TEXT
%----------------------------------------------------------------------------------------

% base class to wrap any text based stuff here. Renders like a paragraph.
% Allows complex commands to be passed, too.
% param 1: *any
\newcommand{\cvtext}[1] {
	\begin{tabular*}{1\mpwidth}{p{0.98\mpwidth}}
		\parbox{1\mpwidth}{#1}
	\end{tabular*}
}

%----------------------------------------------------------------------------------------
%	CV SECTION
%----------------------------------------------------------------------------------------

% Renders a a CV section headline with a nice underline in main color.
% param 1: section title
\newcommand{\cvsection}[2] {
	\vspace{14pt}
	\cvtext{
		\textbf{\LARGE{\textcolor{darkcol}{\uppercase{#1}}}}\\[-4pt]
		\textcolor{#2}{ \rule{0.1\textwidth}{2pt} } \\
	}
}

%----------------------------------------------------------------------------------------
%	META SKILL
%----------------------------------------------------------------------------------------

% Renders a progress-bar to indicate a certain skill in percent.
% param 1: name of the skill / tech / etc.
% param 2: level (for example in years)
% param 3: percent, values range from 0 to 1
\newcommand{\cvskill}[3] {
	\begin{tabular*}{1\mpwidth}{p{0.72\mpwidth}  r}
 		\textcolor{black}{\textbf{#1}} & \textcolor{skillcol}{#2}\\
	\end{tabular*}%
	
	\hspace{4pt}
	\begin{tikzpicture}[scale=1,rounded corners=2pt,very thin]
		\fill [lightcol] (0,0) rectangle (1\mpwidth, 0.15);
		\fill [skillcol] (0,0) rectangle (#3\mpwidth, 0.15);
  	\end{tikzpicture}%
}


%----------------------------------------------------------------------------------------
%	 CV EVENT
%----------------------------------------------------------------------------------------

% Renders a table and a paragraph (cvtext) wrapped in a parbox (to ensure minimum content
% is glued together when a pagebreak appears).
% Additional Information can be passed in text or list form (or other environments).
% the work you did
% param 1: time-frame i.e. Sep 14 - Jan 15 etc.
% param 2: event name (job position etc.)
% param 3: Employer
% param 4: Industry
% param 5: Short description (optional)
% param 6: 
% param 7: 
% param 8: 
\newcommand{\cvevent}[8] {
	
	% we wrap this part in a parbox, so title and description are not separated on a pagebreak
	% if you need more control on page breaks, remove the parbox
	\parbox{\mpwidth}{
		\begin{tabular*}{1\mpwidth}{p{0.72\mpwidth} @{\extracolsep{\fill}}  r}
	 		\textcolor{black}{\textbf{#2}} ~ {#3} & \colorbox{expcol}{\makebox[0.28\mpwidth]{\textcolor{white}{\small{#1}}}} \\
			\textcolor{expcol}{\textbf{#4}} & \\
		\end{tabular*}\\[8pt]
	
		\ifthenelse{\isempty{#5}}{}{
			\cvtext{#5}\\
		}
	}

	\ifthenelse{\isempty{#6}}{}{
		\vspace{9pt}
		{#6}
	}

	\ifthenelse{\isempty{#7}}{}{
		\vspace{9pt}
		{#7}
	}

	\ifthenelse{\isempty{#8}}{}{
		\vspace{9pt}
		{#8}
	}
	\vspace{14pt}
}

%----------------------------------------------------------------------------------------
%	 CV META EVENT
%----------------------------------------------------------------------------------------

% Renders a CV event on the sidebar
% param 1: title
% param 2: subtitle (optional)
% param 3: customer, employer, etc,. (optional)
% param 4: info text (optional)
\newcommand{\cvmetaevent}[4] {
	\textcolor{darkcol} {\cvtext{\textbf{\begin{flushleft}#1\end{flushleft}}}}

	\ifthenelse{\isempty{#2}}{}{
	\textcolor{educol} {\cvtext{\textbf{#2}} }
	}

	\ifthenelse{\isempty{#3}}{}{
		\cvtext{{ {#3} }}\\
	}

	\cvtext{#4}\\[14pt]
}

%---------------------------------------------------------------------------------------
%	QR CODE
%----------------------------------------------------------------------------------------

% Renders a qrcode image (centered, relative to the parentwidth)
% param 1: percent width, from 0 to 1
\newcommand{\cvqrcode}[1] {
	\begin{center}
		\includegraphics[width={#1}\mpwidth]{aaylward-cv-qr.png}
	\end{center}
}

%----------------------------------------------------------------------------------------
%	 CV PUBLICATION
%----------------------------------------------------------------------------------------

% param 1: Date
% param 2: Title
% param 3: Journal
% param 4: Authors 
\newcommand{\cvpublication}[3] {
	
	% we wrap this part in a parbox, so title and description are not separated on a pagebreak
	% if you need more control on page breaks, remove the parbox
	\parbox{\mpwidth}{
		\begin{tabular*}{1\mpwidth}{p{0.72\mpwidth}  r}
	 		\textcolor{black}{#2} & \colorbox{pubcol}{\makebox[0.28\mpwidth]{\textcolor{white}{\small{#1}}}} \\
			\textcolor{pubcol}{\emph{#3}} & \\
		\end{tabular*}\\[8pt]
	}

	\vspace{14pt}
}


%============================================================================%
%
%
%
%	DOCUMENT CONTENT
%
%
%
%============================================================================%
\begin{document}
\columnratio{0.33}
\setlength{\columnsep}{2.2em}
\setlength{\columnseprule}{4pt}
\colseprulecolor{lightcol}
\begin{paracol}{2}
\begin{leftcolumn}
%---------------------------------------------------------------------------------------
%	META IMAGE
%----------------------------------------------------------------------------------------
\includegraphics[width=\linewidth]{aaylward-cv-cropped.jpg}	%trimming relative to image size

%---------------------------------------------------------------------------------------
%	META CONTACT
%----------------------------------------------------------------------------------------

\cvsection{CONTACT}{contactcol}
	
\icontext{MapMarker}{12}{San Diego, CA}{black}\\[6pt]
\icontext{MobilePhone}{12}{(530) 774-6697}{black}\\[6pt]
\iconemail{Envelope}{12}{anthony.aylward@protonmail.com}{anthony.aylward@protonmail.com}{black}\\[6pt]

\vfill\null

%---------------------------------------------------------------------------------------
%	META SKILLS
%----------------------------------------------------------------------------------------
\cvsection{SKILLS}{skillcol}

\cvskill{Python} {7+ yrs} {1} \\[-2pt]

\cvskill{R} {7+ yrs} {1} \\[-2pt]

\cvskill{NGS data} {5+ yrs} {1} \\[-2pt]

\cvskill{Machine learning} {5+ yrs} {0.8} \\[-2pt]

\cvskill{Linux} {5+ yrs} {0.7} \\[-2pt]

\vfill\null

%---------------------------------------------------------------------------------------
%	EDUCATION
%----------------------------------------------------------------------------------------
\cvsection{EDUCATION}{educol}

\cvmetaevent
{Sep 2013 - Jul 2021}
{PhD Bioinformatics \& Systems Biology}
{UC San Diego}
{Dissertation on statistical genetics methods for diabetes research}

\cvmetaevent
{Sep 2008 - Jun 2012}
{BS Mathematics}
{UC Santa Barbara}
{Thesis on mathematical ecology}

%---------------------------------------------------------------------------------------
%	PROFILE
%----------------------------------------------------------------------------------------
\newpage
\vfill\null
\cvsection{PROFILE}{darkcol}

\cvtext{Anthony is a PhD bioinformatics scientist, fascinated by the
ever-growing wealth of knowledge contained in large-scale biological datasets.
His graduate school research focused on statistical genetics and diabetes.\\

When not analyzing data, Anthony enjoys an exciting game of Rocket League or a
relaxing run through the park with his golden retriever, Cali.
}

\end{leftcolumn}
\begin{rightcolumn}

%---------------------------------------------------------------------------------------
%	TITLE  HEADER
%----------------------------------------------------------------------------------------
\fcolorbox{white}{darkcol}{\begin{minipage}[c][3.5cm][c]{1\mpwidth}
	\begin {center}
		\HUGE{ \textbf{ \textcolor{white}{ { Anthony Aylward } } } } \\[-24pt]
		\textcolor{white}{ \rule{0.7\textwidth}{1.25pt} } \\[4pt]
		\large{ \textcolor{white} {Bioinfomatician \& Data Scientist} }
	\end {center}
\end{minipage}} \\[14pt]
\vspace{-12pt}

%---------------------------------------------------------------------------------------
%	QR CODE
%----------------------------------------------------------------------------------------

\begin{tabular*}{1\mpwidth}{@{} p{0.48\mpwidth} @{\extracolsep{\fill}} p{0.3\mpwidth} }
	\cvsection{ONLINE CV}{darkcol} \cvtext{Scan the QR code or
	  \href{https://anthony-aylward.github.io}{\underline{click here}} to view an
		extended online CV.} & \cvqrcode{1} \\
\end{tabular*}

\vspace{-3ex}

%---------------------------------------------------------------------------------------
%	EXPERIENCE
%----------------------------------------------------------------------------------------

\cvsection{EXPERIENCE}{expcol}

\cvevent
	{Sep 2013 - Jul 2021}
	{PhD Student}
	{UC San Diego}
	{Bioinformatics \& Systems Biology}
	{Directly managed two long-term collaborative research projects that led to scientific publications in peer-reviewed journals}
	{\cvtext{\textbf{Bioinformatics / statistics:}}\\ 
	  \cvlist {
		\item Processed hundreds of NGS DNA sequencing datasets using standard tools such as \texttt{bowtie2}, \texttt{bwa}, \texttt{samtools}, \texttt{bedtools}, etc
		\item Developed over a dozen Python packages to facilitate and automate data analysis pipelines
		\item Used R and Bioconductor to perform bioinformatics \& statistical analyses, including developing custom packages
		\item Routinely advised other graduate students and postdocs on statistical analysis questions
	}}
	{\cvtext{\textbf{Machine learning / data science}}\\
	  \cvlist{
		\item Extensively studied and applied machine learning methods as part of graduate course cirriculum and independently. Markov models, random forests, support vector machines, neural networks, etc.
		\item Documented and presented data analysis methods using Jupyter Notebooks
	}}
	{\cvtext{\textbf{Software / computing:}}\\
		\cvlist{
		\item Acted as a primary system administrator for lab computing resources for several years, becoming deeply familiar with Linux and MacOS command line environments. Also capable of working with Windows.  
		\item Made occasional contributions to the design and implementation of the Diabetes Epigenome Atlas, using Python and SQL
	}}

\cvevent
	{Sep 2012 - Sep 2013}
	{Research Assistant}
	{CSU Chico}
	{Mathematical Biology}
	{Developed a mathematical model for angiogenesis (growth of blood vessels) in mouse retina}
	{}
	{}
	{}

%---------------------------------------------------------------------------------------
%	PUBLICATIONS
%----------------------------------------------------------------------------------------

\newpage
\vfill\null
\cvsection{PUBLICATIONS}{pubcol}

\cvtext{\textbf{First author:}}\\

\cvpublication
	{May 2021}
	{\href{https://journals.plos.org/plosgenetics/article?id=10.1371/journal.pgen.1009531}{\textbf{Aylward et al.} Glucocorticoid signaling in pancreatic islets modulates gene regulatory programs and genetic risk of type 2 diabetes}}
	{PLOS Genetics}

\cvpublication
	{Nov 2018}
	{\href{https://academic.oup.com/hmg/advance-article-abstract/doi/10.1093/hmg/ddy314/5164287}{\textbf{Aylward et al.} Shared genetic risk contributes to type 1 and type 2 diabetes etiology}}
	{Human Molecular Genetics}

	\cvtext{\textbf{Other:}}\\

\cvpublication
	{Feb 2021}
	{\href{https://elifesciences.org/articles/59067}{\textbf{Geusz et al.} Pancreatic progenitor epigenome maps prioritize type 2 diabetes risk genes with roles in development}}
	{eLife}

\cvpublication
	{May 2019}
	{\href{https://www.nature.com/articles/s41467-019-09975-4}{\textbf{Greenwald et al.} Pancreatic islet chromatin accessibility and conformation reveals distal enhancer networks of type 2 diabetes risk}}
	{Nature Communications}

\cvpublication
	{Feb 2015}
	{\href{https://academic.oup.com/mbe/article/32/5/1365/1134918}{\textbf{Murrel et al.} Gene-Wide Identification of Episodic Selection}}
	{Molecular Biology and Evolution}

\end{rightcolumn}
\end{paracol}
\end{document}

